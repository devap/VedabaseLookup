#!/usr/bin/env bash

echo "building..."
sam build \
    --profile devap

echo "packaging..."
sam package \
    --profile devap \
    --template-file template.yaml \
    --output-template-file packaged.yaml \
    --s3-bucket aws-lambda-src-govardhana

echo "deploying..."
sam deploy \
    --profile devap \
    --template-file packaged.yaml \
    --capabilities CAPABILITY_IAM \
    --stack-name VedabaseLookup
