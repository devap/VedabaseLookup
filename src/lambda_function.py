import json
import re


def lambda_handler(event, context):  # pylint:disable=W0613

    dest = None
    url_prefix = 'https://vedabase.io/en/library/'

    body_json = {
        'event': event
    }

    resp = {
        'statusCode': 302,
        'headers': {
            'Content-Type': 'application/json',
            'Location': url_prefix

        },
        'body': json.dumps(body_json)
    }

    proxy = event["pathParameters"]["proxy"] if event["pathParameters"] else None
    if proxy:
        # match for Brahma Samhita
        if re.match(r'bs', proxy):
            dest = proxy
            url_prefix = 'https://www.vedabase.com/en/'

        elif re.match(r'[cc|adi|madhya|antya]', proxy):
            dest = proxy if proxy.startswith("cc") else ''.join(['cc/', proxy])
        elif re.match(r'[a-z|A-Z]', proxy):
            # we've got a book of some sort...
            dest = proxy

        else:
            sastra = 'sb' if proxy.count('.') == 2 else 'bg'
            dest = ''.join([sastra, '/', proxy])

        dest = dest.replace('.', '/')
        uri = ''.join([url_prefix, dest])
        resp['headers']['Location'] = uri

    return resp
