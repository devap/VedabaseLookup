from __future__ import print_function
import json

print('Loading function')


def lambda_handler(event, context):
    proxy = None
    sastra = None
    canto = None
    chapter = None
    verse = None
    url_prefix = 'https://vedabase.io/en/library/'

    body_json = {
        'event': event,
        'path_clean': path_clean
    }

    resp = {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'application/json'
        },
        'body': body_json,
    }

    if event and event["pathParameters"]:
        sastra = event["pathParameters"]["book"]
        proxy = event["pathParameters"]["proxy"]
        """
        "proxy": "bg/9.22"
        "proxy": "sb/9.22.12"
        "proxy": "9.22"
        "proxy": "3.22.32"
        """

    if sastra and proxy:
        body_json = {}
        if sastra == 'bg':
            try:
                chapter, verse = proxy.split('.')
                body_json["chapter"] = chapter
                body_json["verse"] = verse
                uri = ''.join([url_prefix, sastra, '/', chapter, '/', verse])
                resp['headers']['Location'] = uri
                resp['statusCode'] = 302
            except ValueError as e:
                body_json["error"] = str(e)

        elif sastra == 'sb':
            try:
                canto, chapter, verse = proxy.split('.')
                body_json["canto"] = canto
                body_json["chapter"] = chapter
                body_json["verse"] = verse
                uri = ''.join([url_prefix, sastra, '/', canto, '/', chapter, '/', verse])
                resp['headers']['Location'] = uri
                resp['statusCode'] = 302
            except ValueError as e:
                body_json["error"] = str(e)

    elif not sastra:
        body_json['sastra'] = 'missing'

    else:
        body_json['verse'] = 'missing'

    if not sastra or not proxy:
        try:
            chapter, verse = path_clean.split('.')
            uri = ''.join([url_prefix, 'bg/', chapter, '/', verse])
            resp['headers']['Location'] = uri
            resp['statusCode'] = 302
        except ValueError as errBG:
            body_json["errorBG"] = str(errBG)
            try:
                canto, chapter, verse = path_clean.split('.')
                uri = ''.join([url_prefix, 'sb/', canto, '/', chapter, '/', verse])
                resp['headers']['Location'] = uri
                resp['statusCode'] = 302
            except ValueError as errSB:
                body_json["errorSB"] = str(errSB)

    resp['body'] = json.dumps(body_json)
    return resp
