import json


def lambda_handler(event, context):

  proxy = None
  sastra = None
  canto = None
  chapter = None
  verse = None
  url_prefix = 'https://vedabase.io/en/library/'

  bodyJSON = {
    'event': event
  }

  resp = {
    'statusCode': 200,
    'headers': {
      'Content-Type': 'application/json'
    },
  }


  if event['path'] == '/':
    resp['headers']['Location'] = url_prefix
    resp['statusCode'] = 302

  elif event["pathParameters"]:
    proxy = event["pathParameters"]["proxy"]

    if proxy:
      if proxy.startswith('bg'):
        sastra = 'bg'
        bodyJSON = {}
        try:
          chapter, verse = proxy.split('/')[1].split('.')
          uri = ''.join([url_prefix, sastra, '/', chapter, '/', verse ])
          resp['headers']['Location'] = uri
          resp['statusCode'] = 302
        except ValueError as e:
          bodyJSON["error"] = str(e)

      elif proxy.startswith('sb'):
        sastra = 'sb'
        try:
          canto, chapter, verse = proxy.split('/')[1].split('.')
          uri = ''.join([url_prefix, sastra, '/', canto, '/', chapter, '/', verse ])
          resp['headers']['Location'] = uri
          resp['statusCode'] = 302
        except ValueError as e:
          bodyJSON["error"] = str(e)

      elif proxy.startswith('nod'):
       sastra = 'nod'
       try:
         chapter = proxy.split('/')[1]
         uri = ''.join([url_prefix, sastra, '/', chapter ])
         resp['headers']['Location'] = uri
         resp['statusCode'] = 302
       except ValueError as e:
         bodyJSON["error"] = str(e)

      elif proxy.startswith('noi'):
       sastra = 'noi'
       try:
         verse = proxy.split('/')[1]
         uri = ''.join([url_prefix, sastra, '/', verse ])
         resp['headers']['Location'] = uri
         resp['statusCode'] = 302
       except ValueError as e:
         bodyJSON["error"] = str(e)

      elif proxy.startswith('iso'):
        sastra = 'iso'
        try:
          verse = proxy.split('/')[1]
          uri = ''.join([url_prefix, sastra, '/', verse ])
          resp['headers']['Location'] = uri
          resp['statusCode'] = 302
        except ValueError as e:
          bodyJSON["error"] = str(e)

      else:
        try:
          sastra = 'bg'
          chapter, verse = proxy.split('.')
          uri = ''.join([url_prefix, sastra, '/', chapter, '/', verse ])
          resp['headers']['Location'] = uri
          resp['statusCode'] = 302
        except ValueError as errBG:
          bodyJSON["errorBG"] = str(errBG)
          try:
            sastra = 'sb'
            canto, chapter, verse = proxy.split('.')
            uri = ''.join([url_prefix, sastra, '/', canto, '/', chapter, '/', verse ])
            resp['headers']['Location'] = uri
            resp['statusCode'] = 302
          except ValueError as errSB:
            bodyJSON["errorSB"] = str(errSB)

  resp['body'] = json.dumps(bodyJSON)
  return resp
